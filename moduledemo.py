import module1
from module1 import long_function_1b
import module_in_folder.module2 as module
from module_in_folder.module3 import PI, GRAVITY

print("Hello module")
module1.function1a()

result = long_function_1b()
print(result)

function_2a = module.function_2a    # without ()
result = function_2a()
print(result)

print("PI is ", PI)
print ("Gravity is {}".format(GRAVITY))