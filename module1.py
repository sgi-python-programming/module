def function1a():
    print("Function 1A called")


def long_function_1b():
    return "function1B"


# without __main__
print("This is module 1")
# if __name__ == "__main__":
#     print("This is module 1")
